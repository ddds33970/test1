from dotenv import load_dotenv
import os
from pymongo import MongoClient

load_dotenv()

class MongoConnector:

    client = MongoClient(os.environ.get("MONGODB_URI"))
    cl = client[os.environ.get("DATABASE_NAME")]

    @classmethod
    def connect(cls) -> MongoClient:
        return cls.cl

    @classmethod
    def disconnect(cls):
        return cls.client.close()

from typing import Optional
from pydantic import BaseModel

class UserModel(BaseModel):
    _id: Optional[str]
    user_name: str
    name: str
    password: str

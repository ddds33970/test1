from repositories.user import UserRepository
from models.user import UserModel
from pymongo.collection import Collection

class UserService:
    def __init__(self, user_repository: UserRepository):
        self.user_repository = user_repository

    def create_user(self, user: UserModel) -> str:
        return self.user_repository.create_user(user)

    def get_user(self, user_id: str) -> UserModel:
        return self.user_repository.get_user(user_id)

    def get_users(self):
        return self.user_repository.get_users()

    def update_user(self, user_id: str, updated_user: UserModel) -> bool:
        return self.user_repository.update_user(user_id, updated_user)

    def delete_user(self, user_id: str) -> bool:
        return self.user_repository.delete_user(user_id)

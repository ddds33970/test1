from dbconnector.mongo_connector import MongoConnector
from fastapi import HTTPException, Depends, APIRouter
from fastapi import FastAPI, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer
from models.user import UserModel
from repositories.user import UserRepository
from services.user import UserService
from typing import List  # Import List from the typing module

router = APIRouter()

@router.post("/users/", response_model=str)
async def create_user(user: UserModel):
    mongo_client = MongoConnector.connect()
    user_repository = UserRepository(mongo_client[os.environ.get("DATABASE_NAME")]["users"])
    user_service = UserService(user_repository)
    user_id = user_service.create_user(user)
    return user_id

@router.get("/users/{user_id}", response_model=UserModel)
async def read_user(user_id: str, user_service: UserService = Depends()):
    user = user_service.get_user(user_id)
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return user

@router.get("/users/", response_model=List[UserModel])
async def read_users(user_service: UserService = Depends()):
    return user_service.get_users()

@router.put("/users/{user_id}", response_model=bool)
async def update_user(user_id: str, updated_user: UserModel, user_service: UserService = Depends()):
    success = user_service.update_user(user_id, updated_user)
    if not success:
        raise HTTPException(status_code=404, detail="User not found")
    return success

@router.delete("/users/{user_id}", response_model=bool)
async def delete_user(user_id: str, user_service: UserService = Depends()):
    success = user_service.delete_user(user_id)
    if not success:
        raise HTTPException(status_code=404, detail="User not found")
    return success

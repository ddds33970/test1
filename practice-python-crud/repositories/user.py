from pymongo.collection import Collection
from models.user import UserModel
from bson.objectid import ObjectId

class UserRepository:
    def __init__(self, collection: Collection):
        self.collection = collection

    def create_user(self, user: UserModel) -> str:
        result = self.collection.insert_one(user.dict())
        return str(result.inserted_id)

    def get_user(self, user_id: str) -> UserModel:
        return self.collection.find_one({"_id": ObjectId(user_id)})

    def get_users(self):
        users = self.collection.find()
        return [UserModel(**user) for user in users]

    def update_user(self, user_id: str, updated_user: UserModel) -> bool:
        result = self.collection.update_one({"_id": ObjectId(user_id)}, {"$set": updated_user.dict()})
        return result.modified_count > 0

    def delete_user(self, user_id: str) -> bool:
        result = self.collection.delete_one({"_id": ObjectId(user_id)})
        return result.deleted_count > 0

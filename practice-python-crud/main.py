import os
from fastapi import FastAPI
from pymongo import MongoClient
from dbconnector.mongo_connector import MongoConnector
from repositories.user import UserRepository
from services.user import UserService
from controller.mainRouter import router

app = FastAPI()

mongo_client = MongoConnector.connect()
# user_repository = UserRepository(mongo_client[os.environ.get("DATABASE_NAME")]["users"])
# user_service = UserService(user_repository)

app.include_router(router, prefix="/api")

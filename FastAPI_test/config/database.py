# โค้ดนี้ใช้เชื่อมต่อกับ MongoDB Atlas cluster โดยดึงจาก pymongo

# นำเข้าคลาส MongoClient จากตัว pymongo.
from pymongo import MongoClient

# การเชื่อมต่อกับ MongoDB Atlas cluster โดยใช้ MongoClient และจัดเตรียมสตริงในการเชื่อมต่อ
client = MongoClient("mongodb+srv://korrawit:5RUIy9DV5pTHvaiQ@cluster0.438eda3.mongodb.net/?retryWrites=true&w=majority")

# เข้าถึงฐานข้อมูล 'test' ภายใน cluster ที่เชื่อมต่อ.
db = client.test

# เข้าถึงคอลเล็กชัน 'users' ภายในฐานข้อมูล 'test'
user_collection = db["users"]
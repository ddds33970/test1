#นำเข้าคลาส 'FastAPI' จากไลบรารี 'FastAPI' และนำเข้า 'router' จากโมดูล 'controller.user '
from fastapi import FastAPI
from controller.user import router

#สร้างอินสแตนซ์ของ FastAPI เพื่อใช้เป็นแอปพลิเคชันของเรา
app = FastAPI()

#รวมรูทเตอร์ (router) เข้ากับแอปพลิเคชัน (app). นั่นหมายถึงเส้นทาง (routes) ที่ถูกกำหนดใน router จะถูกเพิ่มเข้าไปในแอปพลิเคชัน
app.include_router(router)